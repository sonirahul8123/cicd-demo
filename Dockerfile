FROM scratch

LABEL maintaner="Rahul Soni <soni.rahul073@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
